"use strict";

// 1. Кожен об'єкт в JavaScript має свій прототип, який є іншим об'єктом. Якщо якась властивість або метод не знайдено у самому об'єкті, JavaScript буде шукати їх у його прототипі, і так далі по ланцюжку прототипів.
function Animal(name) {
  this.name = name;
}
const cat = new Animal(`Tom`);

Animal.prototype.sayHello = function () {
  console.log(`Hi, my name is ${this.name}`);
};

// cat.sayHello();

function Cat(name, color) {
  Animal.call(this, name);
  this.color = color;
}

Cat.prototype = Object.create(Animal.prototype);
Cat.prototype.constructor = Cat;

Cat.prototype.meow = function () {
  console.log(`Meow`);
};

const myCat = new Cat(`Dan`, `black`);

// myCat.sayHello();
// myCat.meow();

// 2. Коли ми створюємо клас та успадковуємо його від іншого класу (класу-батька), конструктор класу-нащадка може викликати `super()` для виклику конструктора класу-батька. `super()` слугує для виклику конструктора батьківського класу та виконання логіки ініціалізації, яку він включає.

class Animal1 {
  constructor(name) {
    this.name = name;
  }
}

class Cat1 extends Animal1 {
  constructor(name, color) {
    super(name);
    this.color = color;
  }
}

//`super()` викликає конструктор класу-батька

class Animal2 {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}

class Dog extends Animal2 {
  constructor(name, age, breed) {
    super(name, age);
    this.breed = breed;
  }
}

const dog = new Dog(`buddy`, `2`, `лабродор`);

// console.log(dog);

// Якщо конструктор батьківського класу очікує певні аргументи, `super()` дозволяє передати їм значення з конструктора класу-нащадка.

//  Практичні завдання:

// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// 2. Створіть гетери та сеттери для цих властивостей.
// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }

  set name(newName) {
    this._name = newName;
  }
  set age(newAge) {
    this._age = newAge;
  }
  set salary(newSalary) {
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get lang() {
    return this._lang;
  }
  get salary() {
    return super.salary * 3;
  }
}

const programmer1 = new Programmer(`Іван`, 30, 37000, [
  `JS`,
  `Node`,
  `HTML`,
  `CSS`,
]);
const programmer2 = new Programmer(`Віра`, 26, 24000, [`JS`, `HTML`]);
const programmer3 = new Programmer(`Петр`, 28, 40000, [
  `JS`,
  `Node`,
  `HTML`,
  `CSS`,
  `Python`,
]);
const programmer4 = new Programmer(`Іван`, 30, 35000, [`JS`, `Node`, `Python`]);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
console.log(programmer4);
